json.extract! author, :id, :name, :surname, :birth_date, :created_at, :updated_at
json.url author_url(author, format: :json)
